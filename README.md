# Pokémon Trainer on Social Media

This is a social media bot for [Mastodon](https://joinmastodon.org/). It simulates a Pokémon trainer who is travelling through the Hoenn region and posting about their adventures.

Test account:
* https://botsin.space/@test_trainer

## Run the application
[Join any Mastodon server](https://joinmastodon.org/) and register a client and an access token. If you don’t know which Mastodon instance to pick, [botsin.space](https://botsin.space/) is a popular instance only for bots.

Make sure to have the latest version of Node and NPM installed.
```
npm install            # installs/updates dependencies
cp .env-sample .env
$EDITOR .env           # fill in client and access information
npm run-script post               # Creates one post
```

## Disclaimer
This project is NOT affiliated with, funded, or in any way associated with Game Freak nor Nintendo. It does NOT allow you to play any official game. It it a fan-made project that allows to engage with the fandom using Social Media bots.

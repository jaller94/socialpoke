const map = {
  'littleroot-town': {
    name: 'Littleroot Town',
    type: 'town',
    population: 10,
    market: false,
    connections: [
      {
        to: 'route101',
        direction: 'north',
      },
    ]
  },
  'route101': {
    name: 'Littleroot Town',
    type: 'town',
    population: 10,
    market: true,
    pokemon: [
      {
        id: 263,
        chance: 40,
        min_level: 2,
        max_level: 2,
      },
      {
        id: 265,
        chance: 40,
        min_level: 2,
        max_level: 2,
      },
      {
        id: 261,
        chance: 20,
        min_level: 2,
        max_level: 2,
      },
    ],
    connections: [
      {
        to: 'littleroot-town',
        direction: 'north',
      },
    ]
  },
};

'use strict';

function flipCoin(chance = 0.5) {
  return Math.random() < chance;
}

function pickRandom(array) {
  return array[Math.floor(Math.random()*array.length)];
}

function randomInt(min, max) {
  return Math.floor(Math.random()*(max + 1 - min)) + min;
}

module.exports = {
  flipCoin,
  pickRandom,
  randomInt,
};

'use strict';

require('dotenv').config();
const fs = require('fs');
const Mastodon = require('@jaller94/mastodon-api');

const {
  flipCoin,
  randomInt,
  pickRandom,
} = require('./random/pickRandom.js');
const capitalize = require('./string/capitalize.js');

const battleFailed = require('./posts/battle-failed.js');
const battleSuccess = require('./posts/battle-success.js');
const berryPick = require('./posts/berry-pick.js');
const berryPlant = require('./posts/berry-plant.js');
const buy = require('./posts/buy.js');
const teamCatch = require('./posts/team-catch.js');
const teamGoodbye = require('./posts/team-goodbye.js');

function getRandomPokemonZigzagoon() {
  const pokemon = {
    className: 'Zigzagoon',
    gender: flipCoin(0.5) ? 'male' : 'female',
    nickname: null,
    moves: ['Tackle'],
  };
  if (flipCoin(0.8)) pokemon.moves.push('Headbutt');
  if (flipCoin(0.7)) pokemon.moves.push('Pin Missile');
  if (flipCoin(0.4)) pokemon.moves.push('Take Down');
  return pokemon;
}

function getRandomPokemonWumple() {
  const pokemon = {
    className: 'Wumple',
    gender: flipCoin(0.5) ? 'male' : 'female',
    nickname: null,
    moves: ['Tackle'],
  };
  if (flipCoin(0.8)) pokemon.moves.push('Poison Sting');
  return pokemon;
}

function getRandomPokemonSeedot() {
  const pokemon = {
    className: 'Seedot',
    gender: flipCoin(0.5) ? 'male' : 'female',
    nickname: null,
    moves: ['Hidden Power'],
  };
  if (flipCoin(0.8)) pokemon.moves.push('False Swipe');
  if (flipCoin(0.2)) pokemon.moves.push('Razor Wind');
  if (flipCoin(0.05)) pokemon.moves.push('Explosion');
  return pokemon;
}

function getRandomPokemonLunatone() {
  const pokemon = {
    className: 'Lunatone',
    gender: 'genderless',
    nickname: null,
    moves: ['Tackle'],
  };
  if (flipCoin(0.8)) pokemon.moves.push('Confusion');
  if (flipCoin(0.5)) pokemon.moves.push('Rock Throw');
  if (flipCoin(0.3)) pokemon.moves.push('Psychic');
  return pokemon;
}

function getRandomPokemon() {
  const generators = [
    getRandomPokemonZigzagoon,
    getRandomPokemonWumple,
    getRandomPokemonSeedot,
    getRandomPokemonLunatone,
  ];
  return pickRandom(generators)();
}

function tryRenamingEvent(trainer) {
  if (trainer.renamingEvent) return;
  const unnamedMonster = trainer.team.find((monster) => (!monster.nickname));
  trainer.renamingEvent = unnamedMonster;
  return renamingRequest(unnamedMonster);
}

function renamingRequest({monster}) {
  const messages = [
    '%NAME% and I have become such good friends. Any ideas for nicknames?',
    '%NAME% should get a nickname. Can I get some recommendations?',
    'Does anyone have a good nickname idea for %NAME%?',
    'I am looking for a nickname for %NAME%. What would you call %them%?',
    'What nickname would you give a %NAME%?',
    'What nickname should I give %NAME%?',
  ];
  const pronoun = getGenderPronouns(monster.gender);
  let message = pickRandom(messages);
  message = message.replace(/%NAME%/, monster.className);
  message = message.replace(/%they%/, pronoun.subjectPronoun);
  message = message.replace(/%They%/, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/, pronoun.objectPronoun);
  message = message.replace(/%Them%/, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/, pronoun.reflexive);
  message = message.replace(/%Themselves%/, capitalize(pronoun.reflexive));
  return message;
}

function renamingSuccess({monster}) {
  const messages = [
    'Great idea, %POSTER%! From now on I will call my %CLASSNAME% %NAME%.',
    '%POSTER% convinced me that %NAME% is a great name for my %CLASSNAME%.',
    'From now on my %CLASSNAME% gets called %NAME%. Thanks for the suggestion, %POSTER%.',
    'Brilliant, %POSTER%! I will call my %CLASSNAME% %NAME%!',
    '%NAME% – what a nickname for a %CLASSNAME%. Thanks for the idea, %POSTER%.',
    '%POSTER% really caught my attention with the idea of calling my %CLASSNAME% %NAME%. Make it so!',
    '%NAME%, my %CLASSNAME%, likes %their% new nickname. Thanks for the suggestion, %POSTER%.',
    'I am going to ask %POSTER% more often for nickname ideas. %NAME% is a great name for my %CLASSNAME%!',
  ];
  const pronoun = getGenderPronouns(monster.gender);
  let message = pickRandom(messages);
  message = message.replace(/%NAME%/, monster.className);
  message = message.replace(/%they%/, pronoun.subjectPronoun);
  message = message.replace(/%They%/, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/, pronoun.objectPronoun);
  message = message.replace(/%Them%/, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/, pronoun.reflexive);
  message = message.replace(/%Themselves%/, capitalize(pronoun.reflexive));
  return message;
}

function getMonsterLabel(monster) {
  let genderEmoji = '';
  if (monster.gender === 'female') genderEmoji = '♀';
  if (monster.gender === 'male') genderEmoji = '♂';
  if (monster.nickname) {
    return `${monster.nickname}${genderEmoji} (${monster.className})`;
  } else {
    return `${monster.className}${genderEmoji}`;
  }
}

function updatePokemons() {
  updateFields([
    {
      name: 'main pokémon',
      value: (trainer.team.length >= 1) ? getMonsterLabel(trainer.team[0]) : '- none -',
    },
    {
      name: 'side pokémon',
      value: (trainer.team.length >= 2) ? getMonsterLabel(trainer.team[1]) : '- none -',
    },
    {
      name: 'bot father',
      value: '@jaller94@mastodonten.de',
    },
  ]);
  save();
}

function act() {
  const number = randomInt(1, 501);
  let type = '';
  let params = {};
  if (number <= 100) {
    type = 'buy';
  } else if (number <= 350) {
    type = 'battle:success';
    params['monster'] = trainer.team[0];
  } else if (number <= 400) {
    type = 'battle:failed';
    params['monster'] = trainer.team[0];
  } else if (number <= 450) {
    type = 'berry:plant';
  } else if (number <= 500) {
    type = 'berry:pick';
  } else if (number <= 501) {
    if (trainer.team.length === 1) {
      type = 'team:catch';
      const monster = getRandomPokemon();
      params['monster'] = monster;
      trainer.team.unshift(monster);
    } else {
      type = 'team:goodbye';
      params['monster'] = trainer.team[1];
      trainer.team.splice(1);
    }
    updatePokemons();
  }
  generatePost(type, params).then((post) => {
    if (post) {
      toot(post);
    }
  }).catch(console.error);
}

async function generatePost(type, params) {
  const postTypes = {
    'buy': buy,
    'battle:failed': battleFailed,
    'battle:success': battleSuccess,
    'berry:plant': berryPlant,
    'berry:pick': berryPick,
    'team:catch': teamCatch,
    'team:goodbye': teamGoodbye,
  };
  if (!(type in postTypes)) throw Error(`Unknown post type: ${type}`);
  const details = await postTypes[type].generateRandomEvent(params);
  return postTypes[type].generatePost(details);
}

function toot(post, id) {
  const params = {
    language: 'eng',
    status: post.text,
    sensitive: false,
    visibility: 'public',
  }
  if (id) {
    params.in_reply_to_id = id;
  }
  console.log(`toot: ${params.status}`);
  if (/%\S+%/.test(params.status)) {
    console.error(`Placeholders found!\nIn toot: ${params.status}`);
    return;
  }
  if (process.env.ENVIRONMENT === 'production') {
    M.post('statuses', params, (error, data) => {
      if (error) {
        console.error(error);
      } else {
        //fs.writeFileSync('data.json', JSON.stringify(data, null, 2));
        //console.log(data);
        console.log(`ID:${data.id} and timestamp:${data.created_at} `);
        console.log(data.content);
      }
    });
  }
}

function updateFields(array) {
  const params = {
    'fields_attributes[0][name]': array[0].name,
    'fields_attributes[0][value]': array[0].value,
    'fields_attributes[1][name]': array[1].name,
    'fields_attributes[1][value]': array[1].value,
    'fields_attributes[2][name]': array[2].name,
    'fields_attributes[2][value]': array[2].value,
  }
  console.log(`update fields:`);
  if (array.length > 0) console.log(`- ${array[0].name}: ${array[0].value}`);
  if (array.length > 1) console.log(`- ${array[1].name}: ${array[1].value}`);
  if (array.length > 2) console.log(`- ${array[2].name}: ${array[2].value}`);
  if (array.length > 3) console.log(`- ${array[3].name}: ${array[3].value}`);
  if (process.env.ENVIRONMENT === 'production') {
    M.request('PATCH', 'accounts/update_credentials', params, (error, data) => {
      if (error) {
        console.error(error);
      } else {
        //fs.writeFileSync('data.json', JSON.stringify(data, null, 2));
        //console.log(data);
        console.log('Updated the profile fields.');
        console.dir(data);
      }
    });
  }
}

function updateNote(note) {
  const params = {
    'note': note,
  }
  console.log(`update note: ${note}`);
  if (process.env.ENVIRONMENT === 'production') {
    M.request('PATCH', 'accounts/update_credentials', params, (error, data) => {
      if (error) {
        console.error(error);
      } else {
        //fs.writeFileSync('data.json', JSON.stringify(data, null, 2));
        //console.log(data);
        console.log('Updated the profile note.');
        console.dir(data);
      }
    });
  }
}

function save() {
  const data = {
    trainer,
  };
  fs.writeFileSync('save.json', JSON.stringify(data, null, 2));
  console.log(data);
}

const M = new Mastodon({
  client_key: process.env.CLIENT_KEY,
  client_secret: process.env.CLIENT_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
  api_url: process.env.API_URL, // optional, defaults to https://mastodon.social/api/v1/
});

let trainer = {};

try {
  const content = fs.readFileSync('save.json', 'utf8');
  const data = JSON.parse(content);
  trainer = data.trainer;
} catch(err) {
  console.warn('Unable to load a save.');
  console.warn(err);
  trainer = {
    className: 'Picknicker',
    gender: 'female',
    name: 'Charlotte',
    money: 3000,
    team: [getRandomPokemonZigzagoon()],
  };
}

updatePokemons();

if (process.env.ENVIRONMENT === 'development') {
  for (let i = 0; i < 100; i++) {
    act();
  }
} else {
  act();
}

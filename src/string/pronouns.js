'use strict';

const Gender = {
  female: 'female',
  male: 'male',
  genderless: 'genderless',
};

function getGenderPronouns(gender) {
  if (gender === Gender.male) {
    return {
      subjectPronoun: 'he',
      objectPronoun: 'him',
      possessiveDeterminer: 'his',
      possessivePronoun: 'his',
      reflexive: 'himself',
    };
  }
  if (gender === Gender.female) {
    return {
      subjectPronoun: 'she',
      objectPronoun: 'her',
      possessiveDeterminer: 'her',
      possessivePronoun: 'hers',
      reflexive: 'herself',
    };
  }
  return {
    subjectPronoun: 'it',
    objectPronoun: 'it',
    possessiveDeterminer: 'its',
    possessivePronoun: 'it',
    reflexive: 'itself',
  };
}

module.exports = getGenderPronouns;

'use strict';

function capitalize(string) {
  return string.substring(0,1).toUpperCase() + string.substring(1).toLowerCase();
}

module.exports = capitalize;

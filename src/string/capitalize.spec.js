const capitalize = require('./capitalize.js');

test('tests', () => {
  expect(capitalize('aal')).toBe('Aal');
  expect(capitalize('AAL')).toBe('Aal');
  expect(capitalize('aAl')).toBe('Aal');
  expect(capitalize('Aal und')).toBe('Aal und');
});

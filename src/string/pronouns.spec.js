const getGenderPronouns = require('./pronouns.js');

test('tests', () => {
  expect(getGenderPronouns('female').subjectPronoun).toBe('she');
  expect(getGenderPronouns('male').subjectPronoun).toBe('he');
  expect(getGenderPronouns('genderless').subjectPronoun).toBe('it');
});

'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const {
  berries,
  softSoil,
} = require('../data.js');

const messageTemplates = [
  'I wish I had seen these %BERRIES% on %ROUTE% in bloom. Anyway, I picked the berries.',
  'Hoenn is full of berries for me to pick. Like these %BERRIES% on %ROUTE%.',
  'Berries are cool. These %BERRIES% on %ROUTE% look interesting.',
  'More fully grown %BERRIES% on %ROUTE% for me to pick.',
  'I picked %BERRIES% on %ROUTE%, but I actually don’t know what they do.',
  'I am always surprised by how different berries look. I picked %BERRIES% on %ROUTE%.',
  'I picked the %BERRIES% on %ROUTE%. The soft soil is available for new berries.',
  'More berries for me. These %BERRIES% from %ROUTE% are what I needed for my next Pokéblocks.',
  'Is there a Contest Hall near %ROUTE%. I picked fresh %BERRIES% for Pokéblocks.',
];

async function generateRandomEvent() {
  return {
    berry: pickRandom(berries),
    messageTemplate: pickRandom(messageTemplates),
    route: pickRandom(softSoil),
  }
}

async function generatePost({
  berry,
  messageTemplate,
  route,
}) {
  let message = messageTemplate;
  message = message.replace(/%BERRY%/g, berry);
  message = message.replace(/%BERRIES%/g, berry.replace('Berry', 'Berries'));
  message = message.replace(/%ROUTE%/g, route);
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

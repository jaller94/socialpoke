'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const capitalize = require('../string/capitalize.js');
const getGenderPronouns = require('../string/pronouns.js');

const messageTemplates = [
  'I caught %name% to train a new Pokémon.',
  '%Name% will be my new main Pokémon. I just caught %them%.',
  'To challenge myself I caught %name%. I will do my best to train it.',
];

async function generateRandomEvent({monster}) {
  return {
    messageTemplate: pickRandom(messageTemplates),
    monster: monster,
  }
}

async function generatePost({
  messageTemplate,
  monster,
}) {
  let message = messageTemplate;
  const name = `a wild ${monster.className}`;
  const pronoun = getGenderPronouns(monster.gender);
  message = message.replace(/%name%/g, name);
  message = message.replace(/%Name%/g, capitalize(name));
  message = message.replace(/%they%/g, pronoun.subjectPronoun);
  message = message.replace(/%They%/g, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/g, pronoun.objectPronoun);
  message = message.replace(/%Them%/g, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/g, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/g, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/g, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/g, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/g, pronoun.reflexive);
  message = message.replace(/%Themselves%/g, capitalize(pronoun.reflexive));
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

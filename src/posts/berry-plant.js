'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const {
  berries,
  softSoil,
} = require('../data.js');

const messageTemplates = [
  'What a great day for some gardening. I planted a %BERRY% on %ROUTE%.',
  'This %BERRY% I planted on %ROUTE% will take some days to grow.',
  'Hoenn became just a bit more beautiful by me planting a %BERRY% on %ROUTE%.',
  'I hope I will find the time to see the %BERRY% I planted on %ROUTE% bloom.',
  'Does anyone know how long it takes for a %BERRY% to bloom? I planted one on %ROUTE%.',
  'Many trainers will see this %BERRY% I planted on %ROUTE% and I hope it will make them happy.',
  'Flowers enrich the environment. That’s why I planted a %BERRY% on %ROUTE%.',
  'I can’t wait for my %BERRY% on %ROUTE% to fully grow. I need berries for Pokéblocks.',
  'Tap, tap. Grow strong, %BERRY%. %ROUTE% shall be your new home.',
  'My shovel broke while planting a %BERRY% on %ROUTE%, but I guess that’s worth the outcome.',
  'I dislike the rain, but the %BERRY% I planted on %ROUTE% will love it.',
  'The weather could be better for a berry to grow. I hope the %BERRY% I planted on %ROUTE% will make it.',
  '%ROUTE% looks awesome. I couldn’t resist to stop for planting a %BERRY%.',
  'After taking a long time to decide which berry fits %ROUTE%, I went with a %BERRY%.',
  '1. Dig a hole.\n2. Plant your %BERRY%.\n3. Fill the hole.\n4. Use your watering can.',
  'This new %BERRY% fits %ROUTE% nicely. Good job, me!',
  'I planted a %BERRY% on %ROUTE%. I should vist more often.',
  'Nature is beautiful. To give back, I planted a %BERRY% on %ROUTE%.',
  'Mother nature, take this %BERRY% I planted on %ROUTE% and shine!',
  'There is more to a complete training than just fighting. Enjoy the %BERRY% I planted on %ROUTE%.',
  'Planting a berry is less work than I remembered. Please visit my %BERRY% on %ROUTE%.',
  'Whenever I see soft soil I count my berries. A %BERRY% is now going to grow on %ROUTE%.'
];

async function generateRandomEvent() {
  return {
    berry: pickRandom(berries),
    messageTemplate: pickRandom(messageTemplates),
    route: pickRandom(softSoil),
  }
}

async function generatePost({
  berry,
  messageTemplate,
  route,
}) {
  let message = messageTemplate;
  message = message.replace(/%BERRY%/g, berry);
  message = message.replace(/%BERRIES%/g, berry.replace('Berry', 'Berries'));
  message = message.replace(/%ROUTE%/g, route);
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

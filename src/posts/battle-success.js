'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const capitalize = require('../string/capitalize.js');
const getGenderPronouns = require('../string/pronouns.js');
const {
  pokemons,
} = require('../data.js');

const messageTemplates = [
  '%NAME% beat %ENEMY% with %MOVE%.',
  '%NAME% defeated %ENEMY% with %MOVE%.',
  'What’s as cool as %NAME% finishing a battle with %MOVE%?',
  'We just defeated %ENEMY%. It was actually a tough battle.',
  '%NAME% won another battle. I am so proud of %them%.',
  'Way to win a battle, %NAME%! Another glorious win for us!',
  'Me and %NAME% are having a great training against %ENEMY%.',
  '%NAME% gained a lot of experience battling %ENEMY%.',
  'I loved it when %NAME% used %MOVE% perfectly to defeat %ENEMY%.',
  '%NAME% is gaining so much experience today!',
  'Me and %NAME% will show it to %ENEMY% that stole my coins!',
  'Feeling good after %NAME% defeated %ENEMY%.',
  'No time to rest, %NAME%. More training is ahead!',
  'I think %NAME% did an awesome job defeating %ENEMY%.',
  'I’ve got to give recognition for your skill when using %MOVE%, %NAME%!',
  'Maybe we should move on after defeating %ENEMY%.',
  'There was no sign of tiredness when %NAME% defeated %ENEMY%.',
  '%NAME% is getting great at using %MOVE%. %They% just defeated %ENEMY%.',
  'Can %ENEMY% recover when %MOVE% hits too hard? My %NAME% got super strong…',
  'That is going to hurt for a while. %NAME% performed a perfect hit of %MOVE% on %ENEMY%.',
  'A great battle against %ENEMY% is over. %NAME% brought it down using %MOVE%.',
  'Great. %NAME% defeated %ENEMY%. We are getting stronger and stronger.',
  'After %NAME% defeated %ENEMY% we celebrated %their% performance.',
  'Nice job, %NAME%! We managed to defeat %ENEMY%.',
  '%MOVE% by %NAME% brought %ENEMY% down. That was quite the fight!',
  '%NAME% is on fire! After just two turns %they% defeated %ENEMY% using %MOVE%.',
  '%Enemy% had no chance against %NAME% and me. We work so well in a team.',
  '%Enemy% put up quite the fight, however, %NAME% and me are a great team.',
];

async function generateRandomEvent({monster}) {
  return {
    enemy: `a wild ${pickRandom(pokemons)}`,
    messageTemplate: pickRandom(messageTemplates),
    monster: monster,
    move: pickRandom(monster.moves),
  }
}

async function generatePost({
  enemy,
  messageTemplate,
  monster,
  move,
}) {
  let message = messageTemplate;
  const name = monster.className;
  const pronoun = getGenderPronouns(monster.gender);
  message = message.replace(/%NAME%/g, name);
  message = message.replace(/%ENEMY%/g, enemy);
  message = message.replace(/%Enemy%/g, capitalize(enemy));
  message = message.replace(/%they%/g, pronoun.subjectPronoun);
  message = message.replace(/%They%/g, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/g, pronoun.objectPronoun);
  message = message.replace(/%Them%/g, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/g, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/g, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/g, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/g, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/g, pronoun.reflexive);
  message = message.replace(/%Themselves%/g, capitalize(pronoun.reflexive));
  message = message.replace(/%MOVE%/g, move);
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const capitalize = require('../string/capitalize.js');
const getGenderPronouns = require('../string/pronouns.js');
const {
  pokemons,
} = require('../data.js');

const messageTemplates = [
  'Oh no! %NAME% and I lost against %enemy%. We have lots to learn.',
  '%Enemy% defeated %NAME%. I failed it as a trainer today.',
  'Where is the next Poké Center? %Enemy% defeated %NAME%.',
  'We gave everything, but it was not enough. %NAME% got deafeated by %enemy%.',
  'Oh snap. %Enemy% landed a critical hit, defeating %NAME%.',
  'It was a close call but %enemy% defeated %NAME%. Any tips how to defeat %enemy%?',
  'We should have picked a different route. %NAME% got defeated by %enemy%.',
  '%NAME% and I were not strong enough to defeat %enemy%. What to do?',
  '%NAME% got defeated by %enemy%. Who knew they can be this strong?',
  '%Enemy% was stronger than %NAME% and me, but we will train and seek revenge!',
  'I hope %NAME% is ok. %Enemy% landed a critical hit on %them%.',
  'We are heading to the next town after %enemy% took down %NAME%. I have to heal my Pokémon.',
  'That was a bit too much. %Enemy% defeated my %NAME%. We will be back though!',
];

async function generateRandomEvent({monster}) {
  return {
    enemy: `a wild ${pickRandom(pokemons)}`,
    messageTemplate: pickRandom(messageTemplates),
    monster: monster,
  }
}

async function generatePost({
  enemy,
  messageTemplate,
  monster,
}) {
  let message = messageTemplate;
  const name = monster.className;
  const pronoun = getGenderPronouns(monster.gender);
  message = message.replace(/%NAME%/g, name);
  message = message.replace(/%enemy%/g, enemy);
  message = message.replace(/%Enemy%/g, capitalize(enemy));
  message = message.replace(/%they%/g, pronoun.subjectPronoun);
  message = message.replace(/%They%/g, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/g, pronoun.objectPronoun);
  message = message.replace(/%Them%/g, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/g, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/g, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/g, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/g, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/g, pronoun.reflexive);
  message = message.replace(/%Themselves%/g, capitalize(pronoun.reflexive));
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

'use strict';

const {
  randomInt,
  pickRandom,
} = require('../random/pickRandom.js');
const {
  fakeItems,
  items,
  towns,
} = require('../data.js');

const messageTemplates = [
  'Wow… Did you know the Poké Mart of %TOWN% has %ITEMS% back in stock?',
  'I’ve been stopping at a Poké Mart of %TOWN% to buy %ITEM%.',
  'Where would I be, if the Poké Mart did not have %ITEM%?',
  'Yes, I finally stocked up on %ITEMS% here in %TOWN%.',
  'Have %ITEMS% always been this expensive in %TOWN%??',
  'Sweet! The Poké Mart of %TOWN% offers cheap %ITEMS%.',
  'I can always count on the Poké Mart of %TOWN% to have %ITEMS%.',
  'Got lucky! The Poké Mart in %TOWN% offers %ITEMS%.',
  'When shopping in %TOWN%, %ITEM% is my most favored item.',
  'Did they always carry %ITEMS% in %TOWN%?',
  'I bought %AMOUNT% %ITEMS% while being in %TOWN%.',
  'During my break in %TOWN% I bought %AMOUNT% %ITEMS%.',
  'If you buy %AMOUNT% %ITEMS% in %TOWN%, you get a free %FAKEITEM%!',
  'Nice! I found a %FAKEITEM% in the Poké Mart of %TOWN%.',
];

const catchPhrases = [
  'Better be prepared!',
  'You never know what’s coming!',
  'Do your best!',
  'This should come in handy soon!',
  'On to my next adventure!',
  'Gotta catch them all!',
  'This should do the trick!',
  'Better safe than sorry!',
  'I’m feeling much more prepared now!',
  'Better be going!',
  'Looking forward to our next battle!',
  'The journey continues!',
  'Feeling pumped!',
  'Nobody can stop me now!',
  'Waiting for a challenge!',
];

async function generateRandomEvent() {
  return {
    ending: pickRandom(catchPhrases),
    item: pickRandom(items),
    town: pickRandom(towns),
    messageTemplate: pickRandom(messageTemplates),
    amount: randomInt(2, 5),
    fakeItem: pickRandom(fakeItems),
  }
}

async function generatePost({
  ending,
  item,
  town,
  messageTemplate,
  amount,
  fakeItem,
}) {
  let message = messageTemplate;
  const selectedItems = `${item}s`;
  item = /^(a|e|i|o|u|hono|hour)/i.test(item) ? `an ${item}` : `a ${item}`;
  message = message.replace(/%ITEM%/g, item);
  message = message.replace(/%ITEMS%/g, selectedItems);
  message = message.replace(/%TOWN%/g, town);
  message = message.replace(/%AMOUNT%/g, amount);
  message = message.replace(/%FAKEITEM%/g, fakeItem);
  return {
    text: `${message} ${ending}`,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

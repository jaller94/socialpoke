'use strict';

const {
  pickRandom,
} = require('../random/pickRandom.js');
const capitalize = require('../string/capitalize.js');
const getGenderPronouns = require('../string/pronouns.js');

const messageTemplates = [
  '%NAME% and I had a good time, but it’s time to train new Pokémon. Goodbye, %NAME%!',
  'It’s so hard to say goodbye but %NAME% and I are both looking for new challenges. Farewell, friend!',
  '%NAME% and I decided it would be best to train with someone new. I am going to miss %them%.',
  'Goodbyes are never easy. %NAME% and I decided that %they% are able to learn more with a different trainer.',
  '%NAME% left my team. It’s time to look for new challenges!',
  'Farewell, %NAME%. We had a great time but can both learn more with someone else.',
];

async function generateRandomEvent({monster}) {
  return {
    messageTemplate: pickRandom(messageTemplates),
    monster: monster,
  }
}

async function generatePost({
  messageTemplate,
  monster,
}) {
  let message = messageTemplate;
  const name = monster.className;
  const pronoun = getGenderPronouns(monster.gender);
  message = message.replace(/%NAME%/g, name);
  message = message.replace(/%they%/g, pronoun.subjectPronoun);
  message = message.replace(/%They%/g, capitalize(pronoun.subjectPronoun));
  message = message.replace(/%them%/g, pronoun.objectPronoun);
  message = message.replace(/%Them%/g, capitalize(pronoun.objectPronoun));
  message = message.replace(/%their%/g, pronoun.possessiveDeterminer);
  message = message.replace(/%Their%/g, capitalize(pronoun.possessiveDeterminer));
  message = message.replace(/%theirs%/g, pronoun.possessivePronoun);
  message = message.replace(/%Theirs%/g, capitalize(pronoun.possessivePronoun));
  message = message.replace(/%themselves%/g, pronoun.reflexive);
  message = message.replace(/%Themselves%/g, capitalize(pronoun.reflexive));
  return {
    text: message,
  };
}

module.exports = {
  generateRandomEvent,
  generatePost,
};

const mod = require('./buy.js');

test('tests', async() => {
  const params = await mod.generateRandomEvent();
  const post = await mod.generatePost(params);
  expect(post.text).toBeTruthy();
});

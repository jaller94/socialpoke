'use strict';

require('dotenv').config();
const fs = require('fs');
const Mastodon = require('@jaller94/mastodon-api');

const M = new Mastodon({
  client_key: process.env.CLIENT_KEY,
  client_secret: process.env.CLIENT_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
  api_url: process.env.API_URL, // optional, defaults to https://mastodon.social/api/v1/
});

const listener = M.stream('streaming/user');
listener.on('error', err => console.log(err));

listener.on('message', msg => {
  // fs.writeFileSync(`data${new Date().getTime()}.json`, JSON.stringify(msg, null, 2));
  // console.log('user event');
  // console.log(msg);
  if (msg.event === 'notification') {
    const acct = msg.data.account.acct;
    if (msg.data.type === 'follow') {
      console.log(`follow from: ${acct}`);
    } else if (msg.data.type === 'mention') {
      const content = msg.data.status.content;
      const id = msg.data.status.id;
      console.log(`mention: ${id} ${content}`);
    }
  }
});

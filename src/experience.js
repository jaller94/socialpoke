'use strict';

function floor(x) {
  return Math.floor(x + 0.0000001);
}

module.exports = {
  exp2level: {
    mediumFast: (exp) => floor(Math.pow(exp, 1/3)),
  },
  level2exp: {
    fast: (level) => 4*Math.pow(level, 3)/5,
    mediumFast: (level) => Math.pow(level, 3),
    mediumSlow: (level) => (
      1.2*Math.pow(level, 3)
      + 15*Math.pow(level, 2)
      + 100*level
      - 140
    ),
    slow: (level) => 5*Math.pow(level, 3)/4,
  },
};

'use strict';

function validateGender(gender) {
  return ['female', 'male', 'genderless'].includes(gender);
}

class Monster {
  constructor(obj) {
    if (!validateMonsterClass(obj.className)) {
      throw Error(`Unknown monster class name: ${obj.className}`);
    }
    if (!validateGender(obj.gender)) {
      throw Error(`Unknown gender: ${obj.gender}`);
    }

    this.class = obj.className;
    this.gender = obj.gender;
    this.nickname = obj.nickname;
    this.experience = obj.experience;
    this.moves = [
      ...this.moves,
    ];
  }
  
  getLevel() {
    // Medium Fast
    
  }

  toJSON() {
    return {
      className: this.className,
      gender: this.gender,
      nickname: this.nickname,
      experience: this.experience,
    }
  }
}

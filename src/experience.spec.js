const {
  exp2level,
  level2exp,
} = require('./experience.js');

describe('exp2level', () => {
  test('Medium Fast', () => {
    const fn = exp2level.mediumFast;
    expect(fn(1)).toBe(1);
    expect(fn(8)).toBe(2);
    expect(fn(26)).toBe(2);
    expect(fn(27)).toBe(3);
    expect(fn(63)).toBe(3);
    expect(fn(64)).toBe(4);
    expect(fn(124)).toBe(4);
    expect(fn(125)).toBe(5);
  });
});

describe('level2exp', () => {
  test('Medium Fast', () => {
    const fn = level2exp.mediumFast;
    expect(fn(1)).toBe(1);
    expect(fn(2)).toBe(8);
    expect(fn(3)).toBe(27);
    expect(fn(4)).toBe(64);
    expect(fn(5)).toBe(125);
  });
});
